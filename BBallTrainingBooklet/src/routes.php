<?php

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/demo', DemoController::class . ':index')->setName('demo');

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});



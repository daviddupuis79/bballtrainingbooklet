<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;

class BaseController  {

    protected $container;
    
    protected $logger;
    protected $renderer;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;

        $this->logger = $this->container->get('logger');
        $this->renderer = $this->container->get('renderer');
    }

    public function __get($property) {
		if ($this->container->{$property}) {
			return $this->container->{$property};
		}
	}
}
<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class DemoController extends BaseController {

    public function index(Request $request, Response $response, $args) {
        $this->logger->info("Demo");
        return $this->renderer->render($response, 'demo.phtml', $args);
    }
}
